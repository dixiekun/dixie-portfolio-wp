<?php 
/*
@package dixierpacheco theme
404 page
*/

get_header( ); ?>

 <div class="wrapper">
		<div class="header" style="background-image: url(
                                        <?php if( get_field('header_background_image', get_option('page_for_posts')) ): ?>

                                        <?php the_field('header_background_image', get_option('page_for_posts')); ?>

                                        <?php endif; ?>
                                                        );">
        <div class="color-overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						    <h1 class="alt-title">Sorry, page not found!</h1>
						    <h3 class="sub-title">It looks like nothing is here.</h3>
						    <a href="#" class="scroll-down" address="true"></a>		
					    </div>
				</div>
			</div>
        </div>
		</div>
		</div>


		<div class="main main-raised">
	            <div class="container">
					<h2 class="error-404 not-found animated fadeInDown" >404 error</h2>


					

<?php get_footer( ); ?>    
