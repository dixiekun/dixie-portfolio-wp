<?php 
/*
@package dixierpacheco

*/

get_header( ); ?>

 <div class="wrapper">
		<div class="header" style="background-image: url(<?php echo get_theme_mod('ht_image'); ?>);">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1 class="typer"><strong><i data-typer-targets="<?php echo get_theme_mod( 'typer_data', 'Logo,UI/UX,IA,Web,Mobile app,Strategy,Wordpress' ); ?>"><?php echo get_theme_mod( 'i_default', 'UI/UX' ); ?></i></strong> <?php echo get_theme_mod( 'showcase_h1', 'DESIGNER' ); ?></h1>
						<h3><?php echo get_theme_mod( 'showcase_h3', "Finding the synergy between the users' needs and the business goals." ); ?></h3>
						<a href="#" class="scroll-down" address="true"></a>		
					</div>
				</div>
			</div>

		</div>
		</div>


		<div class="main main-raised">
	            <div class="container">
					<h2 class="fadeInDown" ><?php _e('My Works'); ?></h2>

									<?php
							 			$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

                                        $args = array(
                                            'post_type' => 'portfolio',
                                            'post_per_page' => 4,
											'paged' => $paged,
											'page' => $paged
                                            );

                                        $loop = new WP_Query( $args );  
									?>

                                       <?php if( $loop->have_posts()): ?>

									   <!--the loop-->
                                        <?php while( $loop->have_posts()): $loop->the_post(); ?>

											<?php	get_template_part('template-parts/portfolio-content', 'portfolio'); ?>

                                        <?php endwhile; ?>

										<!--// Pagination here-->
										<div class="portfolio-nav">
											<div class="portfolio-prev">
												<?php echo get_next_posts_link( '<i class="fa fa-angle-left"></i> Older Entries', $loop->max_num_pages ); // display older posts link ?>						
											</div>

											<div class="portfolio-next">
												<?php echo get_previous_posts_link( 'Newer Entries <i class="fa fa-angle-right"></i>' ); // display newer posts link ?>
											</div>
										</div>

										<?php wp_reset_postdata(); ?>	

                                        <?php endif; ?>

					
					<div class="section skills-section">
						<h2 class="title text-center"><?php _e('Capabilities', 'dixierpacheco'); ?></h2>
						<div class="row">

								<?php if(is_active_sidebar( 'skills-area-1' )): ?>
                                	<?php dynamic_sidebar( 'skills-area-1' ); ?>
                            	<?php endif; ?>


								<?php if(is_active_sidebar( 'skills-area-2' )): ?>
                                	<?php dynamic_sidebar( 'skills-area-2' ); ?>
                            	<?php endif; ?>
					

								<?php if(is_active_sidebar( 'skills-area-3' )): ?>
                                	<?php dynamic_sidebar( 'skills-area-3' ); ?>
                            	<?php endif; ?>


								<?php if(is_active_sidebar( 'skills-area-4' )): ?>
                                	<?php dynamic_sidebar( 'skills-area-4' ); ?>
                            	<?php endif; ?>

						</div>
					</div>

                   
<?php get_footer( ); ?>    
