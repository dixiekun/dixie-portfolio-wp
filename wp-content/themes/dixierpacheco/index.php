<?php 
/*
@package dixierpacheco theme
Standard single page
*/

get_header( ); ?>

 <div class="wrapper">
		<div class="header" style="background-image: url(
                                        <?php if( get_field('header_background_image', get_option('page_for_posts')) ): ?>

                                        <?php the_field('header_background_image', get_option('page_for_posts')); ?>

                                        <?php endif; ?>
                                                        );">
        <div class="color-overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						    <h1 class="alt-title"><?php the_field('headline_text', get_option('page_for_posts')); ?></h1>
						    <h3 class="sub-title"><?php the_field('sub_title', get_option('page_for_posts')); ?></h3>
						    <a href="#" class="scroll-down" address="true"></a>		
					    </div>
				</div>
			</div>
        </div>
		</div>
		</div>


		<div class="main main-raised">
	            <div class="container">
					<h2 class="animated fadeInDown" ><?php single_post_title(); ?></h2>

						<?php   

											if( have_posts()):
												while( have_posts()): the_post(); 

													get_template_part('template-parts/content', get_post_format() );

												endwhile;

											endif;    

						?>

					

<?php get_footer( ); ?>    
