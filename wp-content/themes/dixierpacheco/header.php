
<!doctype html>
<html <?php language_attributes( ); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/assets/img/apple-icon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="description" content="<?php bloginfo('description'); ?>">

	<title>
        <?php bloginfo( 'name' ); ?> | 
        <?php is_front_page() ? bloginfo( 'description' ): wp_title( ); ?> 
    </title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" >
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

	<!-- CSS Files -->
	
	<style type="text/css">
	<?php print get_option( 'drp_customcss' ); ?>
	</style>
	
    <?php wp_head(); ?>

</head>

<?php if ( is_front_page()):
		$body_classes = array('index-page');
	  elseif( is_home()):
	  	$body_classes = array('blog-page', 'project-page');
	  else: 
	  	$body_classes = array('project-page');
	  endif;

 ?>
	


<body <?php body_class($body_classes); ?>>
<?php if ( is_front_page() && !is_paged()): ?>
<div class="se-pre-con" style="	background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/drp-preloader.gif) center no-repeat #031019; "></div>
<?php endif; ?>
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
	<div class="container">
        <div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	    	</button>
	    	<a href="#">
	        	<div class="animated pulse logo-container">
	                <div class="logo">

						<?php if ( get_theme_mod( 'drp_logo' ) ) : ?>
    						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
 
        					<img src="<?php echo get_theme_mod( 'drp_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="tooltip" title="<b>Hello.</b> Let's design something <b>awesome!</b>" data-placement="right" data-html="true">
 
    						</a>
 
    					<?php else : ?>
               
							<hgroup>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
								<p class="site-description"><?php bloginfo( 'description' ); ?></p>
							</hgroup>
									
						<?php endif; ?>
						
	                </div>
	            </div>
	      	</a>
	    </div>

        <?php
						wp_nav_menu( array(
                            'menu'              => 'primary',
							'theme_location'    => 'primary',
							'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'container_id'      => 'navigation-index',
							'menu_class'        => 'nav navbar-nav',
                            'menu_id'           => 'menu-primary-navigation-en',
							'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
							'walker'            => new WP_Bootstrap_Navwalker()
						) );
		?>
  
	</div>
</nav>
<!-- End Navbar -->