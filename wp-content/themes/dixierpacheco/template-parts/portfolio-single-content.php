<?php 
/*
@package dixierpacheco theme

portfolio single content format
*/
?>
                    <div class="section folio" >
						<div class="row">
							<div class=" col-md-4">
								<div class="client-logo">
                                    <h2>
                                        <?php if( get_field('client_logo') ): ?>

                                        <img src="<?php the_field('client_logo'); ?>" />

                                        <?php endif; ?>
                                    </h2>    
                                </div>								
																			
							</div>
							<div class="col-md-8">
								<div class="details">
									<h2><?php the_title(); ?></h3>
									<p class="project-description"><?php the_content(); ?></p>
								</div>
							</div>						
						</div>                      
					</div>
                    <div class="extend">
                        <?php if( get_field('stylescape') ): ?>

                                        <img src="<?php the_field('stylescape'); ?>" class="styleboard" />

                        <?php endif; ?>
                    </div>

                    <div class="section folio">
                        <div class="gallery">
                        <?php 

                        $images = get_field('gallery_images');

                        if( $images ): ?>
                                <?php foreach( $images as $image ): ?>                                    
                                    <?php if( !empty($image['url']) ){ ?><a href="<?php echo $image['url']; ?>"><?php } ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" title="<?php echo $image['title']; ?>">
                                    <?php if( !empty($image['url']) ){ ?></a><?php } ?>
                                <?php endforeach; ?>
                        <?php endif; ?>
                        </div>    
                    </div>  

					<div class="portfolio-nav">
						<div class="portfolio-prev">
                           <?php $prev_post = get_adjacent_post(false, '', true);
                            if(!empty($prev_post)) {
                            echo '<a href="' . get_permalink($prev_post->ID) . '" rel="tooltip" data-placement="top" data-original-title="Previous Portfolio"><i class="fa fa-angle-left"></i></a>'; }
                            ?>							
						</div>

						<div class="portfolio-next">
                            <?php $next_post = get_adjacent_post(false, '', false);
                            if(!empty($next_post)) {
                            echo '<a href="' . get_permalink($next_post->ID) . '" rel="tooltip" data-placement="top" data-original-title="Next Portfolio"><i class="fa fa-angle-right"></i></a>'; }
                            ?>
						</div>
					</div>