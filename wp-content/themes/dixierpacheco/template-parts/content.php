<?php 
/*
@package dixierpacheco theme

Blog page content format
*/
?>
                    <div class="section folio" >
                        <div class="row">
							<div class="col-md-8">
								
								
								<div class="image-overlay">
									<a class="overlay-text" href="<?php the_permalink( ); ?>"><?php _e('Details'); ?></a>
								</div>

                                <?php if( has_post_thumbnail( )): ?>
								    <?php the_post_thumbnail('full', array('class' => 'featured')); ?>
								<?php endif; ?>
																			
							</div>
							<div class="col-md-4">
								<div class="details">
									<h3 class="project-title"><a href="<?php the_permalink( ); ?>"><?php the_title( ); ?></a></h3>
									<p class="project-category">
                                        <?php $category = get_the_category();
                                             echo $category[0]->cat_name;
                                        ?>
                                    </p>
									<p class="project-description"><?php the_excerpt(); ?> </p>
									<a class="read-more btn" href="<?php the_permalink( ); ?>"><?php _e('Read more ->'); ?></a>
								</div>
							</div>						
						</div>
                    </div>