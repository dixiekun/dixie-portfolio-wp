<?php 
/*
@package dixierpacheco theme

Standard single page content format
*/
?>
                
                    <div class="section">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <p><?php the_content(); ?></p>
                        </div>
                        <div class="gallery">
                        <?php 

                        $images = get_field('gallery_images');

                        if( $images ): ?>
                                <?php foreach( $images as $image ): ?>                                    
                                    <?php if( !empty($image['url']) ){ ?><a href="<?php echo $image['url']; ?>"><?php } ?>
                                    <img class="featured drop-shadow" src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" title="<?php echo $image['title']; ?>">
                                    <?php if( !empty($image['url']) ){ ?></a><?php } ?>
                                <?php endforeach; ?>
                        <?php endif; ?>
                        </div> 
                    </div>  

                        