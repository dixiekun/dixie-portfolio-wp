<?php 
/*
@package dixierpacheco theme

About page content template
*/
?>
                

                    <div class="section folio" >
						<div class="row">
                            <div class="col-md-8">
								<div class="details">
									<h2><?php the_title(); ?></h2>
									<p class="project-description"><?php echo get_the_content(); ?></p>
								</div>
							</div>
							<div class=" col-md-4">
                                <h2 class="hidden-xs"><?php _e('&nbsp;'); ?></h2>
								<button class="btn btn-primary btn-raised" onclick="window.location.href='<?php the_field('resume'); ?>'" id="resume"><i class="material-icons">description</i><?php _e('My Resume'); ?> 
								</button>																										
							</div>												
						</div>                      
					</div>

                    <div class="section the-process">
                    <div class="extend" style="background-image: url(
                        <?php if( get_field('the_process_section_image') ): ?>

                                        <?php the_field('the_process_section_image'); ?>

                        <?php endif; ?>
                                        );">
					<h2 class="big-head center"><?php the_field('the_section_text'); ?></a>
                    </div>
                    <div class="process">
                        <?php the_field('after_the_content'); ?>
                    </div>
                    <div class="section folio">
						<h2 class="title"><?php _e('Clients');?></h2>

                        <?php if( have_rows('clients') ): ?>
                        <div class="row clients">
                            <?php while( have_rows('clients') ): the_row(); 

                            // vars
                            $clientlogo = get_sub_field('client_logo');
                            $clientname = get_sub_field('client_name');
                            $clientniche = get_sub_field('client_niche');

                            ?>
                            <div class="col-xs-4 col-md-2">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
	                                <div class="flipper">
                                        
		                                <div class="front">
                                        <div class="pad">
			                            <!-- front content -->
                                        <img src="<?php echo   $clientlogo['url']; ?>" alt="<?php echo   $clientlogo['alt'] ?>" />
                                        </div>
		                                </div>
		                                <div class="back">
                                        <div class="pad">                    
			                            <!-- back content -->
                                        <h5><?php echo $clientname; ?></h5>
                                        <p><?php echo $clientniche; ?></p>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <?php endwhile; ?>

                        </div>
                        <?php endif; ?>

                    </div>
                

