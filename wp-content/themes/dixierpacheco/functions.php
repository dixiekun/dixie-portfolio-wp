<?php 
/*
@package dixierpacheco

*/

/* 
    ========================
    Include scripts and stylesheets files
    ========================
*/
function drp_script_enqueue() {
    $appjspath = get_stylesheet_directory() . '/assets/js/app.min.js';
    $drpjspath = get_stylesheet_directory() . '/assets/js/app.min.js';

    wp_enqueue_style( 'drp_style', get_stylesheet_uri(), array(), filemtime( get_stylesheet_uri() ), 'all' );
    wp_enqueue_script( 'drp_app_js', get_template_directory_uri() . '/assets/js/app.min.js', array(),filemtime( $appjspath ), true );
    wp_enqueue_script( 'drp_js', get_template_directory_uri() . '/assets/js/drp.js', array(), filemtime( $drpjspath ), true );
    $translation_array = array( 'theme_path' => get_template_directory_uri() );
    wp_localize_script( 'drp_js', 'drp_site', $translation_array );
    
}

add_action( 'wp_enqueue_scripts', 'drp_script_enqueue' );

    // Register nav walker 
    require_once get_template_directory() .'/inc/wp-bootstrap-navwalker.php';
    //Register Customizer
    require get_template_directory() . '/inc/customizer.php';
    //Register Function Admin
    require get_template_directory() . '/inc/function-admin.php';
    //Register Enqueue
    require get_template_directory() . '/inc/enqueue.php';
    //Register Theme Support options
    require get_template_directory() . '/inc/theme-support.php';
    //Register Custom post types
    require get_template_directory() . '/inc/custom-post-type.php';
    //Register Shortcode options
    require get_template_directory() . '/inc/shortcodes.php';
    //Register AJAX Functions
    require get_template_directory() . '/inc/ajax.php';
    //Register Pagination Functions
    require get_template_directory() . '/inc/pagination.php';

    // Theme Support
    function drp_theme_setup() {
        // Nav Menus
        register_nav_menus( array(
            'primary' => __('Primary Menu')
        ) );

    }

    add_action( 'after_setup_theme', 'drp_theme_setup' );

    //Post Thumbnails support
    add_theme_support( 'post-thumbnails' ); 



/* 
    ================
    Custom Post Type
    ================
*/
function drp_custom_post_type () {

    $labels = array(
        'name' => 'Portfolio',
        'singular_name' => 'Portfolio',
        'add_new' => 'Add Portfolio Item',
        'all_items' => 'All Items',
        'add_new_item' => 'Add Item',
        'edit_item' => 'Edit Item',
        'new_item' => 'New Item',
        'view_item' => 'View Item',
        'search_item' => 'Search Portfolio',
        'not_found' => 'No items found',
        'not_found_in_trash' => 'No items found in trash',
        'parent_item_colon' => 'Parent Item'

    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
        ),
        //'taxonomies' => array('category', 'post_tag'),
        'menu_position' => 5,
        'menu_icon' => 'dashicons-portfolio',
        'exclude_from_search' => false
    );
    register_post_type('portfolio', $args);
}

add_action( 'init', 'drp_custom_post_type' );

/* 
    ================
    Custom Taxonomies
    ================
*/
function drp_custom_taxonomies (){
    //add new taxonomy hierarchical
    $labels = array(
        'name' => 'Portfolio-categories',
        'singular_name' => 'Portfolio-category',
        'search_items' => 'Search Portfolio-categories',
        'all_items' => 'All Portfolio-categories',
        'parent_item' => 'Parent Portfolio-category',
        'parent_item_colon' => 'Parent Portfolio-category:',
        'edit_item' => 'Edit Portfolio-category',
        'update_item' => 'Update Portfolio-category',
        'add_new_item' => 'Add New Portfolio-category',
        'new_item_name' => 'New Portfolio-category Name',
        'menu_name' => 'Portfolio-category' 
    ); 

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'portfolio-category')
    );

    register_taxonomy( 'portfolio-category', array('portfolio'), $args );

    //add new taxonomy not hierarchical
    
    register_taxonomy( 'software', 'portfolio', array(
            'label' => 'Software',
            'rewrite' => array( 'slug' => 'software'),
            'hierarchical' => false
    ) );
}

add_action( 'init', 'drp_custom_taxonomies' );


/* 
    ================
    SVG upload support
    ================
*/

function svg_mime_types( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;}
add_filter( 'upload_mimes', 'svg_mime_types' );

function svg_size() {
  echo '<style>
    svg, img[src*=".svg"] {
      max-width: 150px !important;
      max-height: 150px !important;
    }
  </style>';
}
add_action('admin_head', 'svg_size');



/* 
    ================
    Widgets locations
    ================
*/

function drp_init_widgets($id){

    //Skills Widget Area
    register_sidebar( array(
        'name'          => __('Skills Area 1', 'dixierpacheco'),
        'id'            => 'skills-area-1',
        'description'   => 'Put your html text here, see documentation on how to use progress circle & icon.',
        'before_widget' => '<div class="col-md-6 skill">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''      

    ) );

    register_sidebar( array(
        'name'          => __('Skills Area 2', 'dixierpacheco'),
        'id'            => 'skills-area-2',
        'description'   => 'Put your html text here, see documentation on how to use progress circle & icon.',
        'before_widget' => '<div class="col-md-6 skill">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''      

    ) );

    register_sidebar( array(
        'name'          => __('Skills Area 3', 'dixierpacheco'),
        'id'            => 'skills-area-3',
        'description'   => 'Put your html text here, see documentation on how to use progress circle & icon.',
        'before_widget' => '<div class="col-md-6 skill">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''      

    ) );

    register_sidebar( array(
        'name'          => __('Skills Area 4', 'dixierpacheco'),
        'id'            => 'skills-area-4',
        'description'   => 'Put your html text here, see documentation on how to use progress circle & icon.',
        'before_widget' => '<div class="col-md-6 skill">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''      

    ) );


    //Call to Action Widget Area
    register_sidebar( array(
        'name'          => __('Call-to-action', 'dixierpacheco'),
        'id'            => 'call-to-action',
        'description'   => 'Call to action above the contact form.',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2 class="text-center title">',
        'after_title'   => '</h2>'      

    ) );


}

add_action('widgets_init', 'drp_init_widgets' );



