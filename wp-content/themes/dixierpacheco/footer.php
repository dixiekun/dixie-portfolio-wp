 <div class="section landing-section">
	                	<div class="row">
	                    	<div class="col-md-8 col-md-offset-2">

							<img class="profile-picture" src="<?php print get_option( 'profile_picture') ?>">

	                        <?php if(is_active_sidebar( 'call-to-action' )): ?>
                                <?php dynamic_sidebar( 'call-to-action' ); ?>
                            <?php endif; ?>

	                        <form action="#" method="post" id="drpContactForm" class="contact-form" data-url="<?php echo admin_url('admin-ajax.php'); ?>">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating is-empty">
											<label class="control-label"><?php _e('Your Name'); ?></label>
											<input type="text" class="form-control" name="name" id="name" >
										<span class="material-input"></span>
										<small class="text-danger animated bounce form-control-msg"><?php _e('Your Name is Required.'); ?></small>
										</div>			
									</div>

									<div class="col-md-6">
										<div class="form-group label-floating is-empty">
											<label class="control-label"><?php _e('Your Email'); ?></label>
											<input type="email" class="form-control" name="email" id="email">
										<span class="material-input"></span>
										<small class="text-danger animated bounce form-control-msg"><?php _e('Your Email is Required.'); ?></small>
										</div>
									</div>
								</div>

								<div class="form-group label-floating is-empty">
									<label class="control-label"><?php _e('Your Message'); ?></label>
									<textarea class="form-control" rows="4" name="message" id="message"></textarea>
								<span class="material-input"></span>
								<small class="text-danger animated bounce form-control-msg"><?php _e('A Message is Required.'); ?></small>
								</div>
								<div class="row">
									<div class="col-md-4 col-md-offset-4 text-center">
										<button class="btn btn-primary btn-raised">
											<?php _e('Send Message'); ?>
										</button>
									</div>
									<small class="text-info animated pulse form-control-msg js-form-submission"><?php _e('Submission in process, please wait...'); ?></small>
									<small class="text-success animated rollIn form-control-msg js-form-success"><?php _e('Message successfully submitted, thank you!'); ?></small>
									<small class="text-danger animated bounce form-control-msg js-form-error"><?php _e('There was a problem with the Contact Form, please try again.'); ?></small>
								</div>

							</form> <!--end of form-->		

	                    	</div>
	                	</div>
	            	</div>

	            </div>
	        </div>
		</div>

    </div>


<footer class="footer">
        <div class="container">
            <div class="copyright">
                &copy; <?php echo Date('Y'); ?> dixieraizpacheco.com
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>

<a href="#0" class="cd-top" style="background: rgba(15, 118, 211, 0.8) url(<?php echo get_template_directory_uri(); ?>/assets/img/back-to-top-arrow.svg) no-repeat center 50%;">Top</a>
</body>
	<!--   Core JS Files   -->

  	<script>
			$(window).on('load', function(){
				// Animate loader off screen
				$('.se-pre-con').delay(1000).slideUp('slow', 'swing');;
				});

	</script>


</html>
