<?php 

function drp_customize_register( $wp_customize ) {

    $wp_customize->add_setting( 'drp_logo' ); // Add setting for logo uploader
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'drp_logo', array(
        'label'    => __( 'Upload Logo', 'drp' ),
        'section'  => 'title_tagline',
        'settings' => 'drp_logo',
    ) ) );

    //hero typer showcase section
        $wp_customize -> add_section('showcase', array(
            'title' => __('Hero Typer', 'drp'),
            'description' => sprintf(__('Options for Hero typer', 'drp')),
            'priority'    => 150
        ));

        $wp_customize -> add_setting('ht_image', array(
            'default'   => get_bloginfo( 'template_directory' ).'/assets/img/herobg-05.jpg',
            'type'      => 'theme_mod'
        ));

        $wp_customize -> add_control(new WP_Customize_Image_Control($wp_customize, 'ht_image', array(
            'label'     => __('Hero Bg Image', 'drp'),
            'section'   => 'showcase',
            'settings'  => 'ht_image',
            'priority'  => 1
        )));

        $wp_customize -> add_setting('typer_data', array(
            'default'   => _x('Logo,UI/UX,IA,Web,Mobile app,Strategy,Wordpress', 'drp'),
            'type'      => 'theme_mod'
        ));

        $wp_customize -> add_control('typer_data', array(
            'label'     => __('Typer Data', 'drp'),
            'section'   => 'showcase',
            'priority'  => 2
        ));

        $wp_customize -> add_setting('i_default', array(
            'default'   => _x('UI/UX', 'drp'),
            'type'      => 'theme_mod'
        ));

        $wp_customize -> add_control('i_default', array(
            'label'     => __('default text', 'drp'),
            'section'   => 'showcase',
            'priority'  => 3
        ));

        $wp_customize -> add_setting('showcase_h1', array(
            'default'   => _x('DESIGNER', 'drp'),
            'type'      => 'theme_mod'
        ));

        $wp_customize -> add_control('showcase_h1', array(
            'label'     => __('h1 text', 'drp'),
            'section'   => 'showcase',
            'priority'  => 4
        ));

        $wp_customize -> add_setting('showcase_h3', array(
            'default'   => _x("Finding the synergy between the users' needs and the business goals.", 'drp'),
            'type'      => 'theme_mod'
        ));

        $wp_customize -> add_control('showcase_h3', array(
            'label'     => __('sub heading text', 'drp'),
            'section'   => 'showcase',
            'priority'  => 5
        ));
}

add_action( 'customize_register', 'drp_customize_register' );