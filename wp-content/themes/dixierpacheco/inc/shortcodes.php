<?php
/*
@package dixierpacheco theme
    ========================
    Shortcode Options
    ========================
*/

//add shortcode to nav menu items
add_filter('wp_nav_menu_items', 'do_shortcode');
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


//Tooltip shortcode 
function drp_tooltip( $atts, $content = null ){
    
    //get the attributes
    $atts = shortcode_atts( 
        array(
            'placement' =>  'top',
            'title'     =>  '',
        ),
        $atts,
        'tooltip'

     );

     $title = ( $atts['title'] == '' ? $content : $atts['title'] );

     //return HTML

     return '<span class="drp-tooltip" data-toggle="tooltip" data-placement="'. $atts['placement'] .'" title="'. $title .'">'. $content .'</span>';

}

add_shortcode( 'tooltip', 'drp_tooltip' );  





//Contact Form Shortcode
function drp_contact_form( $atts, $content = null ){
    
    //[contact_form]
    //get the attributes
    $atts = shortcode_atts( 
        array(),
        $atts,
        'contact_form'

     );

     //return HTML
     ob_start();
     include 'templates/contact-form.php';
     return ob_get_clean();

}

add_shortcode( 'contact_form', 'drp_contact_form' );


//Progress Circle Shortcode
// function drp_progress_circle( $atts, $content = null ){

//     //[progress_circle]
    
//     //get the attributes
//     $atts = shortcode_atts( 
//         array(
//             'progress' =>  '1',
//         ),
//         $atts,
//         'progress_circle'

//      );

//      //return HTML

//      return '<div id="pc-design" data-prog-val="'.$atts['progress']. '"></div>';

// }

// add_shortcode( 'progress_circle', 'drp_progress_circle' );  

