<?php
/*
@package dixierpacheco theme
    ========================
    Theme Support Options
    ========================
*/

$options = get_option( 'post_formats');
$formats = array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat');
$output = array();
    foreach ( $formats as $format) {
        $output[] = ( @$options[$format] == 1 ? $format : '' );
    }
if( !empty( $options)) {
    add_theme_support( 'post-formats', $output );
}



//Test Email Server 
// function mailtrap($phpmailer) {
//   $phpmailer->isSMTP();
//   $phpmailer->Host = 'smtp.mailtrap.io';
//   $phpmailer->SMTPAuth = true;
//   $phpmailer->Port = 2525;
//   $phpmailer->Username = 'd1b6597fb79b7a';
//   $phpmailer->Password = '822f23998afcd2';
// }

// add_action('phpmailer_init', 'mailtrap');