<?php
/*
@package dixierpacheco theme
    ========================
    Admin Enqueue functions
    ========================
*/

function drp_load_admin_scripts($hook) {
    //echo $hook;

    if ( 'toplevel_page_drp_theme' == $hook ) {
           
            wp_register_style( 'drp_admin', get_template_directory_uri() . '/assets/css/drp.admin.css', array(), '1.0.0', 'all' );
            wp_enqueue_style( 'drp_admin' );

            wp_enqueue_media( );

            wp_register_script( 'drp-admin-script', get_template_directory_uri() . '/assets/js/drp.admin.js', array('jquery'),'1.0.0', true );
            wp_enqueue_script( 'drp-admin-script');
    
    } else if ( 'drp-options_page_drp_theme_custom_css' == $hook ) {

            wp_enqueue_style( 'ace', get_template_directory_uri() . '/assets/css/drp.ace.css', array(), '1.0.0', 'all' );

            wp_enqueue_script( 'ace', get_template_directory_uri() . '/assets/js/ace/ace.js', array('jquery'),'1.2.6', true );
            wp_enqueue_script( 'drp-custom-css-script', get_template_directory_uri() . '/assets/js/drp.custom_css.js', array('jquery'), '1.0.0', true);
    }

    else { return; }
}
add_action( 'admin_enqueue_scripts', 'drp_load_admin_scripts');