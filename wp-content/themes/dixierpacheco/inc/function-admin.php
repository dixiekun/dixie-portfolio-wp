<?php
/*
@package dixierpacheco theme
    ========================
    Admin Page
    ========================
*/

function drp_add_admin_page() {

    //Generate drp admin page
    add_menu_page( 'DRP Theme Options', 'DRP Options', 'manage_options', 'drp_theme', 'drp_theme_create_page', get_template_directory_uri() . '/assets/img/drpicon.png', 8 );

    //Generate drp sub pages
    add_submenu_page( 'drp_theme', 'DRP Profile Options', 'Profile', 'manage_options', 'drp_theme', 'drp_theme_create_page' );
    add_submenu_page( 'drp_theme', 'DRP Theme Support Options', 'Theme Support Options', 'manage_options', 'drp_theme_support_options', 'drp_theme_support_options_page' );
    add_submenu_page( 'drp_theme', 'DRP Contact Form', 'Contact Form', 'manage_options', 'drp_theme_contact_form', 'drp_theme_contact_form_page' );
    add_submenu_page( 'drp_theme', 'DRP CSS Options', 'Custom CSS', 'manage_options', 'drp_theme_custom_css', 'drp_theme_custom_css_page' );


    //Activate custom settings
    add_action( 'admin_init', 'drp_custom_settings' );
}
add_action( 'admin_menu', 'drp_add_admin_page' );

function drp_custom_settings() {
//Profile Options
    register_setting( 'drp-settings-group', 'profile_picture');
    register_setting( 'drp-settings-group', 'first_name');
    register_setting( 'drp-settings-group', 'last_name');

    add_settings_section( 'drp-profile-options', '', 'drp_sidebar_options', 'drp_theme' );

 // Add Current Image Preview
    add_settings_field('sidebar_image_preview', 'Image Preview', 'drp_sidebar_profile_picture_preview', 'drp_theme', 'drp-profile-options');
    add_settings_field( 'sidebar-profile-picture', 'Profile Picture', 'drp_sidebar_profile_picture', 'drp_theme', 'drp-profile-options');
    add_settings_field( 'sidebar-name', 'Full Name', 'drp_sidebar_name', 'drp_theme', 'drp-profile-options');

//Theme Support Options
    register_setting( 'drp-theme-support-options-group', 'post_formats' );

    add_settings_section( 'drp-theme-support-options-section', 'Theme Support Section', 'drp_theme_support_options_section', 'drp_theme_support_options_page' );

    add_settings_field( 'post-formats', 'Post Formats', 'drp_post_formats', 'drp_theme_support_options_page', 'drp-theme-support-options-section');

//Contact Form Options
    register_setting( 'drp-contact-option-group', 'activate_contact' );

    add_settings_section( 'drp-contact-section', 'Contact Form', 'drp_contact_section', 'drp_theme_contact_form_page' );

    add_settings_field( 'activate-form', 'Activate Contact Form', 'drp_activate_contact', 'drp_theme_contact_form_page', 'drp-contact-section');

//Custom CSS Options
    register_setting( 'drp-custom-css-group', 'drp_customcss', 'drp_sanitize_custom_css' );

    add_settings_section( 'drp-custom-css-section', 'Custom CSS', 'drp_custom_css_section_callback', 'drp_theme_custom_css' );

    add_settings_field( 'custom-css', 'Insert your Custom CSS', 'drp_custom_css_callback', 'drp_theme_custom_css', 'drp-custom-css-section');

}

function drp_custom_css_section_callback() {
    echo 'Customize DRP Theme with your own CSS.';
}

function drp_custom_css_callback(){
    $css = get_option( 'drp_customcss' ); 
    $css = ( empty($css) ? '/* DRP Theme Custom CSS */' : $css );
    echo '<div id="customCss">'.$css.'</div><textarea id="drp_customcss" name="drp_customcss" style="display:none; visibility:hidden;">'.$css.'</textarea>';
}


function drp_theme_support_options_section() {
    echo 'Activate and Deactivate specific theme support options.';
}

function drp_contact_section() {
    echo 'Activate and Deactivate the built-in contact form.';
}

function drp_activate_contact(){
    $options = get_option( 'activate_contact' ); 
    $checked = ( @$options == 1 ? 'checked' : '' );
    echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.'/> </label>';
}



function drp_post_formats(){
    $options = get_option( 'post_formats' ); 
    $formats = array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat');
    $output = '';
    foreach ( $formats as $format) {
        $checked = ( @$options[$format] == 1 ? 'checked' : '' );
        $output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.'/> '.$format.'</label><br>';
    }
    echo $output;
}


//Profile Options functions
function drp_sidebar_options(){
    //customize profile information

}

function drp_sidebar_profile_picture_preview(){
    $picture = esc_attr( get_option( 'profile_picture' ));  ?>
    <div id="upload_picture_preview" style="min-height: 100px;">
        <img id="image-preview" style="max-width:300px;" src="<?php echo esc_url( $picture ); ?>" />
    </div>
    <?php
}

function drp_sidebar_profile_picture(){
    $picture = esc_attr( get_option( 'profile_picture') ); 
    echo '
        <input type="button" class="button button-secondary" value="Upload Profile Picture" id="upload-button"> <input type="hidden" id="profile-picture" name="profile_picture" value="'.$picture.'"  /> 
         '; 

}


function drp_sidebar_name(){
    $firstName = esc_attr( get_option( 'first_name') ); 
    $laststName = esc_attr( get_option( 'last_name') );
    echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="First Name" /> 
          <input type="text" name="last_name" value="'.$laststName.'" placeholder="Last Name" />'; 

}



//Sanitization Settings

function drp_sanitize_custom_css( $input ) {
    $output = esc_textarea( $input );
    return $output;
}

//Template submenu functions
function drp_theme_create_page() {
    //generation of admin page
    require_once( get_template_directory() . '/inc/templates/drp-admin.php');

}

function drp_theme_support_options_page() {
    //generation of theme support options page
    require_once( get_template_directory() . '/inc/templates/drp-theme-support-options.php');

}

function drp_theme_contact_form_page() {
    //generation of Contact form options page
    require_once( get_template_directory() . '/inc/templates/drp-contact-form.php');

}

function drp_theme_custom_css_page() {
    //generation of Custom CSS Area
    require_once( get_template_directory() . '/inc/templates/drp-custom-css.php');


}