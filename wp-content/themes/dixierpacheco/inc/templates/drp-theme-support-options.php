<h1>DRP Theme Support Options</h1>
<?php settings_errors( ); ?>

<form method="post" action="options.php">
    <?php settings_fields( 'drp-theme-support-options-group' ); ?>
    <?php do_settings_sections( 'drp_theme_support_options_page' ) ?>
    <?php submit_button( ); ?>
</form>
