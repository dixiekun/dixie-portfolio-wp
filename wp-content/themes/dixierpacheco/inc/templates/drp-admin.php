<h1>DRP Profile Options</h1>
<?php settings_errors( ); ?>
<form method="post" action="options.php">
    <?php settings_fields( 'drp-settings-group' ); ?>
    <?php do_settings_sections( 'drp_theme' ) ?>
    <?php submit_button( ); ?>
</form>
