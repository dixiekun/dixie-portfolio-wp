<h1>DRP Contact Form</h1>
<?php settings_errors( ); ?>

<p>Use this <strong>shortcode</strong> to activate the Contact Form inside a Page or a Post. </p>
<p><code>[contact_form]</code> </p>

<form method="post" action="options.php">
    <?php settings_fields( 'drp-contact-option-group' ); ?>
    <?php do_settings_sections( 'drp_theme_contact_form_page' ) ?>
    <?php submit_button( ); ?>
</form>
