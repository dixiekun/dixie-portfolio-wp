<?php 
/*
@package dixierpacheco theme
portfolio single page
*/

get_header( ); ?>

 <div class="wrapper">
		<div class="header" style="background-image: url(
                                        <?php if( get_field('header_background_image') ): ?>

                                        <?php the_field('header_background_image'); ?>

                                        <?php endif; ?>
                                                        );">
        <div class="color-overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						    <h1 class="project-heading"><?php the_field('headline_text'); ?></h1>
						    <h3 class="proj-head-cat">
								<?php 
										 $category = get_the_terms( $post->ID, 'portfolio-category' );     
											foreach ( $category as $cat){
											echo $cat->name;
											}
                                ?>
                            </h3>
						    <a href="#" class="scroll-down" address="true"></a>		
					    </div>
				</div>
			</div>
        </div>
		</div>
		</div>


		<div class="main main-raised">
	            <div class="container">
                    <?php   

                                        if( have_posts()):
                                            while( have_posts()): the_post(); 

												get_template_part('template-parts/portfolio-single-content', 'single');

                                         	endwhile;

                                        endif;    

                    ?>

					
<?php get_footer( ); ?>    
