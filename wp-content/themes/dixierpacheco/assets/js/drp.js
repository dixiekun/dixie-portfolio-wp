jQuery(document).ready( function($){


    /* Contact Form Submission */
    $('#drpContactForm').on('submit', function(e){
        
        e.preventDefault();

        $('.has-error').removeClass('has-error');
        $('.js-show-feedback').removeClass('js-show-feedback');

        var form = $(this);

        var name = form.find('#name').val(),
            email = form.find('#email').val(),
            message = form.find('#message').val(),
            ajaxurl = form.data('url');


            if( name === '' ){
                $('#name').parent('.form-group').addClass('has-error');
                return;
            }

            if( email === '' ){
                $('#email').parent('.form-group').addClass('has-error');
                return;
            }

            if( message === '' ){
                $('#message').parent('.form-group').addClass('has-error');
                return;
            }

            form.find('input, button, textarea').attr('disabled', 'disabled');
            $('.js-form-submission').addClass('js-show-feedback');


            $.ajax({

                url : ajaxurl,
                type : 'post',
                data : {

                    name : name,
                    email : email,
                    message : message,
                    action: 'drp_save_user_contact_form'

                },
                error : function( response ){
                    $('.js-form-submission').removeClass('js-show-feedback');
                    $('.js-form-error').addClass('js-show-feedback');
                    form.find('input, button, textarea').removeAttr('disabled');
                },
                success : function( response ){
                    if( response == 0) {

                        setTimeout(function(){
                            $('.js-form-submission').removeClass('js-show-feedback');
                            $('.js-form-error').addClass('js-show-feedback');
                            form.find('input, button, textarea').removeAttr('disabled');
                        }, 1500);

                    } else {
                        
                        setTimeout(function(){
                            $('.js-form-submission').removeClass('js-show-feedback');
                            $('.js-form-success').addClass('js-show-feedback');
                            form.find('input, button, textarea').removeAttr('disabled').val('');
                        }, 1500);
                    }

                }

            });


    });

//scrolldown button
 $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('div.main').offset().top }, 'slow');
      return false;
    });
  });

//Animate Profile Picture
    // $('.profile-picture').addClass("hide_me").viewportChecker({
    //     classToAdd: 'visible animated zoomIn',
    //     offset: 100
    //    });


//Back-to-Top Scroll button
// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});


    //Scroll to contact form
    $("a[href='#contact']").on('click',function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
        return false;
});


});



