jQuery(document).ready( function($){

    var mediaUploader;

    $( '#upload-button' ).on('click', function(e) {
        e.preventDefault();
        if( mediaUploader ){
            mediaUploader.open();
            return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose an image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

        mediaUploader.on('select', function(){
            attachment = mediaUploader.state().get('selection').first().toJSON();
            $( '#profile-picture' ).val(attachment.url);
            $( '#image-preview' ).attr("src", attachment.url);
        });

        mediaUploader.open();

    });

});